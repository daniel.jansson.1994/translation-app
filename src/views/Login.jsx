import LoginForm from "../components/Login/LoginForm"
//Login view, connected to the LoginForm component for all interactions.
const Login = () => {
  return (
    <>
      <LoginForm />
    </>
  )
}

export default Login
