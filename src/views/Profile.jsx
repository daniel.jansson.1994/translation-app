import { useEffect } from "react"
import { useNavigate } from "react-router-dom"
import { userById } from "../api/user"
import ProfileActions from "../components/Profile/ProfileActions"
import ProfileHeader from "../components/Profile/ProfileHeader"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"

//View for the profile page.

const Profile = () => {
  const { user, setUser } = useUser()

  useEffect(() => {
    const findUser = async () => {
      const [error, latestUser] = await userById(user.id)
      if (error === null) {
        storageSave(STORAGE_KEY_USER, latestUser)
        setUser(latestUser)
      }
    }
  }, [setUser, user.id])

  return (
    <>
      <ProfileHeader username={user.username} />

      <ProfileTranslationHistory translations={user.translations} />
      <ProfileActions />
    </>
  )
}

export default withAuth(Profile)
