import TranslationsForm from "../components/Translations/TranslationsForm"
import withAuth from "../hoc/withAuth"
import { translationAdd } from "../api/translation"
import { useUser } from "../context/UserContext"
import { storageSave } from "../utils/storage"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import TranslationField from "../components/Translations/TranslationField/TranslationField"

//View for the translation page.

const Translation = () => {
  const { user, setUser } = useUser()
  //The function will update the users storage and add the new word into the database.
  const handleSubmit = async (notes) => {
    const [error, updatedUser] = await translationAdd(user, notes)
    if (error !== null) {
      return
    }

    storageSave(STORAGE_KEY_USER, updatedUser)
    setUser(updatedUser)
  }

  return (
    <>
      <section id="translation-notes">
        <div>
          <TranslationsForm onTranslation={handleSubmit} />
        </div>
      </section>
      <TranslationField />
    </>
  )
}

export default withAuth(Translation)
