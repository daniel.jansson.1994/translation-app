import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { useNavigate } from "react-router-dom"
import { loginUser } from "../../api/user"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageSave } from "../../utils/storage"

const usernameConfig = {
  required: true,
  minLength: 2,
}

const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const { user, setUser } = useUser()

  const navigate = useNavigate()

  const [loading, setLoading] = useState(false)
  const [apiError, setApiError] = useState(null)

  useEffect(() => {
    if (user !== null) {
      navigate("/profile")
    }
  }, [user, navigate])

  const onSubmit = async ({ username }) => {
    setLoading(true)
    const [error, userResponse] = await loginUser(username)
    if (error !== null) {
      setApiError(error)
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse)
      setUser(userResponse)
    }
    setLoading(false)
  }

  const errorMessage = () => {
    if (!errors.username) {
      return null
    }

    if (errors.username.type === "required") {
      return <span>Username is required</span>
    }

    if (errors.username.type === "minLength") {
      return <span>Username is too short :(</span>
    }
  }

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="grid grid-cols-4">
          <label htmlFor="username"></label>
          <input
            className="w-96  text-center mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
            type="text"
            placeholder="username..."
            {...register("username", usernameConfig)}
          />
          {errorMessage}

          <button
            type="submit"
            disabled={loading}
            className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-full mt-2 ml-3  mr-52"
          >
            Login
          </button>
        </div>
        {loading && <p className="text-center text-3xl">Loggin in...</p>}
        {apiError && <p>{apiError}</p>}
      </form>
    </>
  )
}

export default LoginForm
