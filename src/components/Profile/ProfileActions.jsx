import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"
import { translationClearHistory } from "../../api/translation"
//Component for the interactions on the user page.
const ProfileActions = () => {
  const { setUser, user } = useUser()
  //This function will remove the history from sessionStorage and the database.
  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure?\nThis can not be undone!")) {
      return
    }

    const [clearError] = await translationClearHistory(user.id)

    if (clearError !== null) {
      return
    }
    const updatedUser = {
      ...user,
      translations: [],
    }

    storageSave(STORAGE_KEY_USER, updatedUser)
    setUser(updatedUser)
  }
  //The function will let the user be signed out.
  const handleLogoutClick = () => {
    if (window.confirm("Are you sure you want to log out?")) {
      storageDelete(STORAGE_KEY_USER)
      setUser(null)
    }
  }

  return (
    <ul class="columns-2 text-center">
      <li>
        <button
          onClick={handleClearHistoryClick}
          className="bg-yellow-500 hover:bg-yellow-700 text-white font-bold py-2 px-4 ml-80 rounded-full grid-colum"
        >
          Clear history
        </button>
      </li>
      <li>
        <button
          onClick={handleLogoutClick}
          className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-full mr-80 pl-9 pr-9"
        >
          Logout
        </button>
      </li>
    </ul>
  )
}

export default ProfileActions
