//Component for a nice message for the user that logs inn.
const ProfileHeader = ({ username }) => {
  return (
    <header className="bg-stone-400">
      <h4 className="text-4xl pt-5 pb-5">Have a nice stay: {username}</h4>
    </header>
  )
}

export default ProfileHeader
