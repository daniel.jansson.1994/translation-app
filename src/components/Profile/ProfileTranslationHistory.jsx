import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"
//Component for setting up the users history in translations.
const ProfileTranslationHistory = ({ translations }) => {
  const translationList = translations.map((translation, index) => (
    <ProfileTranslationHistoryItem
      key={index + "-" + translation}
      translation={translation}
    />
  ))
  return (
    <section className="text-center text-4xl">
      <h4 className="pb-5">Your translation history:</h4>
      <ul>{translationList}</ul>
    </section>
  )
}

export default ProfileTranslationHistory
