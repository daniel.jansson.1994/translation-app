//Component for displaying the users history in translations.
const ProfileTranslationHistoryItem = ({ translation }) => {
  return <li className="p-1 text-3xl">{translation}</li>
}

export default ProfileTranslationHistoryItem
