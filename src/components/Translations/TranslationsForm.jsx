import { useForm } from "react-hook-form"

//The translation form.
const TranslationsForm = ({ onTranslation }) => {
  const { register, handleSubmit } = useForm()
  //function for on submit from user. To store the current translation to the database.
  const onSubmit = ({ translationNotes }) => {
    if (translationNotes === "") {
      throw new Error("Enter a string.")
    }

    onTranslation(translationNotes)
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="text-center">
      <div className="grid grid-cols-3">
        <label htmlFor="translation-notes" className="text-3xl mt-2 pl-44">
          Select your translation:
        </label>
        <input
          className="w-96  text-center mt-2 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          id="textField"
          type="text"
          {...register("translationNotes")}
          placeholder="Hello World"
        />

        <button
          type="submit"
          className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-full mt-2  mr-52"
        >
          Translate!
        </button>
      </div>
    </form>
  )
}

export default TranslationsForm
