//function for saving the current session
export const storageSave = (key, value) => {
  sessionStorage.setItem(key, JSON.stringify(value))
  if (!key) {
    throw new Error("storageSave: missing key")
  }
  if (!value) {
    throw new Error("storageSave: missing value")
  }
}
//function for reading the current session
export const storageRead = (key) => {
  const data = sessionStorage.getItem(key)
  if (data) {
    return JSON.parse(data)
  }
  return null
}
//function for deleting the current session
export const storageDelete = (key) => {
  sessionStorage.removeItem(key)
}
