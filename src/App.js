import { BrowserRouter, Route, Routes } from "react-router-dom"
import "./App.css"
import Navbar from "./Navbar/Navbar"
import Login from "./views/Login"
import Profile from "./views/Profile"
import Translation from "./views/Translation"

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/translation" element={<Translation />} />
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App
