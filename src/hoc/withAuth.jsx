import { useUser } from "../context/UserContext"
import { Navigate } from "react-router-dom"

//Checking if a user is set into SessionStorage.
//If not the user will be sent to the login page.
const withAuth = (Component) => (props) => {
  const { user } = useUser()
  if (user !== null) {
    return <Component {...props} />
  } else {
    return <Navigate to="/" />
  }
}

export default withAuth
