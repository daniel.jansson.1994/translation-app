import { NavLink } from "react-router-dom"
import { useUser } from "../context/UserContext"

//Just a navigation bar for the user to move around in the webpage

const Navbar = () => {
  const { user } = useUser()

  return (
    <nav>
      <ul className="bg-gray-400 text-6xl pb-4 text-center">
        <li>Sign word translator!</li>
      </ul>
      {user !== null && (
        <ul class="grid gap-1 grid-cols-2 grid-rows-1 bg-stone-400">
          <li class="text-3xl bg-stone-400 border-2 border-black text-center">
            <NavLink to="/translation">Translation</NavLink>
          </li>
          <li class="text-3xl bg-stone-400 border-2 border-black text-center">
            <NavLink to="/profile">Profile</NavLink>
          </li>
        </ul>
      )}
    </nav>
  )
}

export default Navbar
