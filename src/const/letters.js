export const LETTERS = [
  {
    id: 1,
    name: "A",
    image: "img/a.png",
  },
  {
    id: 2,
    name: "B",
    image: "img/b.png",
  },
  {
    id: 3,
    name: "C",
    image: "img/c.png",
  },
  {
    id: 4,
    name: "D",
    image: "img/d.png",
  },
  {
    id: 5,
    name: "E",
    image: "img/e.png",
  },
  {
    id: 6,
    name: "F",
    image: "img/f.png",
  },
  {
    id: 7,
    name: "G",
    image: "img/g.png",
  },
  {
    id: 8,
    name: "H",
    image: "img/h.png",
  },
  {
    id: 9,
    name: "I",
    image: "img/i.png",
  },
  {
    id: 10,
    name: "J",
    image: "img/j.png",
  },
  {
    id: 11,
    name: "K",
    image: "img/k.png",
  },
  {
    id: 12,
    name: "L",
    image: "img/l.png",
  },
  {
    id: 13,
    name: "M",
    image: "img/m.png",
  },
  {
    id: 14,
    name: "N",
    image: "img/n.png",
  },
  {
    id: 15,
    name: "O",
    image: "img/o.png",
  },
  {
    id: 16,
    name: "P",
    image: "img/p.png",
  },
  {
    id: 17,
    name: "Q",
    image: "img/q.png",
  },
  {
    id: 18,
    name: "R",
    image: "img/r.png",
  },
  {
    id: 19,
    name: "S",
    image: "img/s.png",
  },
  {
    id: 20,
    name: "T",
    image: "img/t.png",
  },
  {
    id: 21,
    name: "U",
    image: "img/u.png",
  },
  {
    id: 22,
    name: "V",
    image: "img/v.png",
  },
  {
    id: 23,
    name: "W",
    image: "img/w.png",
  },
  {
    id: 24,
    name: "X",
    image: "img/x.png",
  },
  {
    id: 25,
    name: "Y",
    image: "img/y.png",
  },
  {
    id: 26,
    name: "Z",
    image: "img/z.png",
  },
]
