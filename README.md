# Translation App

# Author

Daniel Jansson

## Scripts to run:

### `npm install`

### `npm start`

### Developer's note on the standing of the project

The project was scoped by the developer in the wrong way, which led into complications when the function for
displaying the translated words came into play. This function is therefor not in the application, when the developer
discovered the mistake in a late stage of development.

### How would the solution look like?

The string prompted from the user would be split into an array (e.g "Hello" => "H", "e", "l", "l", "o").
This would then let the program display each picture that would fit with the array with the map function.
The word was "trapped" within the app inside of destruction and could not be utilized outside of the onSubmit function.
